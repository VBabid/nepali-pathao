package gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.AutoCompleteTextView;
import android.widget.Toast;
import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class PickDropFragmentMVP {

  public interface RequiredViewOps{
    Context getActivityContext();

    void showToast(Toast toast);

    AutoCompleteTextView getAutoEditTextView(int resId);

    void showClearSearch(boolean show , String type);

  }


  public interface ProvidedPresenterOps{

    void setView(RequiredViewOps view);

    void onDestroy(boolean isChangingConfigurations);

    void onConfigurartionChange(RequiredViewOps view);

    GoogleApiClient configureGoogleClient();

    void setAutoCompleteAdapter(AutoCompleteTextView view);

    void onEditTextClick(String type);
  }

  public interface RequiredPresenterOps{
    Context getActivityContext();

    void updateclearSearchButton(String type);
  }

  public interface ProvidedModelOps{
    void onDestroy(boolean isChangingConfigurations);
  }
}
