package gtav.pathao.nepal.flow.com.nepalipathao.events;

import gtav.pathao.nepal.flow.com.nepalipathao.common.UserLikelyPlace;

/**
 * Created by Vutka Bilai on 5/31/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class UserLocationFoundEvent {

  UserLikelyPlace userLocation ;

  public UserLocationFoundEvent(UserLikelyPlace userLocation) {
    this.userLocation = userLocation;
  }

  public UserLikelyPlace getUserLocation() {
    return userLocation;
  }
}
