package gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.SettingFragmentMVP;

/**
 * Created by Vutka Bilai on 5/17/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class SettingAdapter extends RecyclerView.Adapter<SettingsViewHolder> {

  private SettingFragmentMVP.ProvidedPresenterOps mPresenter;

  // Define listener member variable
  private OnItemClickListener listener;

  // Define the listener interface
  public interface OnItemClickListener {
    void onItemClick(View itemView, int position);
  }

  public void setClickListener(OnItemClickListener listener){
    this.listener = listener ;
  }


  public SettingAdapter() {
  }

  @Override
  public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    return mPresenter.onCreateViewHolder(parent, viewType);
  }

  @Override
  public void onBindViewHolder(SettingsViewHolder holder, int position) {
    mPresenter.onBindViewHolder(holder, position);
  }

  @Override
  public int getItemCount() {
    return mPresenter.getItemCount();
  }

  public void setPresenter(SettingFragmentMVP.ProvidedPresenterOps mPresenter) {
    this.mPresenter = mPresenter;
    notifyDataSetChanged();
  }
}
