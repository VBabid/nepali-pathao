package gtav.pathao.nepal.flow.com.nepalipathao.LoginActivity.view;

import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.accountkit.AccessToken;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitLoginResult;
import com.facebook.accountkit.ui.LoginType;

import butterknife.BindView;
import butterknife.ButterKnife;
import gtav.pathao.nepal.flow.com.nepalipathao.LoginActivity.model.LogInModel;
import gtav.pathao.nepal.flow.com.nepalipathao.LoginActivity.presnter.LogInPresenter;
import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.LoginActivityMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.view.MainActivity;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ActivityFragmentStatemaintainer;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ToastMaker;
import gtav.pathao.nepal.flow.com.nepalipathao.rest.ApiInterface;
import gtav.pathao.nepal.flow.com.nepalipathao.rest.RestServiceGenerator;
import gtav.pathao.nepal.flow.com.nepalipathao.rest.UserloginResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.accountkit.AccountKit.initialize;


/**
 * Created by VutkaBilai on 4/17/17.
 * mail : la4508@gmail.com
 */

public class LoginActivity extends AppCompatActivity implements LoginActivityMVP.RequiredViewOps{

    private final String TAG = getClass().getSimpleName();

    private LoginActivityMVP.ProvidedPresenterOps mPresenter;

    private ActivityFragmentStatemaintainer mStateMaintainer =
            new ActivityFragmentStatemaintainer(getFragmentManager(), TAG);

    @BindView(R.id.btn_signup)
    Button signUp;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setUpMvp();
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (AccountKit.getCurrentAccessToken() != null) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

    }

    public void onLogInClick(View view){
        mPresenter.onLogin(LoginType.PHONE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setUpMvp() {

        try {
            if (mStateMaintainer.isFirstTimeIn()) {

                initialize(this);
            }else {
                reInitialize(this);
            }
        }catch (InstantiationException | IllegalAccessException e){
            Log.e(TAG , "onCreate() "+e.getMessage());

            throw new RuntimeException(e);
        }
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }


    @Override
    public Context getActivityContext() {
        return LoginActivity.this;
    }

    @Override
    public void showProgressDialog(ProgressDialog pDialog) {
        pDialog.show();
    }

    @Override
    public void hideProgressDialog(ProgressDialog pDialog) {
        pDialog.hide();
    }

    @Override
    public void showToast(Toast toast) {
        toast.show();
    }

    @Override
    public void showAlert(AlertDialog alertDialog) {
        alertDialog.show();
    }

    @Override
    public void startActivitywithIntent(Intent intent) {
        startActivity(intent);
    }

    @Override
    public void startActivityForResultWithIntent(Intent intent, int requestCode) {
        startActivityForResult(intent , requestCode);
    }

    @Override
    @TargetApi(23)
    public boolean shouldRequestPermission(String permission) {
        return shouldShowRequestPermissionRationale(permission);
    }

    @Override
    @TargetApi(23)
    public void requestForPermission(String[] permissions, int requestCode) {
        requestPermissions(permissions , requestCode);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode != LogInPresenter.FRAMEWORK_REQUEST_CODE) {
            return;
        }

        final String toastMessage ;
        final AccountKitLoginResult loginResult = AccountKit.loginResultWithIntent(data);
        if (loginResult == null || loginResult.wasCancelled()) {
            toastMessage = "Login Cancelled";
        } else if (loginResult.getError() != null) {
            toastMessage = loginResult.getError().getErrorType().getMessage();

        } else {
            final AccessToken accessToken = loginResult.getAccessToken();
            final long tokenRefreshIntervalInSeconds =
                    loginResult.getTokenRefreshIntervalInSeconds();
            if (accessToken != null) {
                mPresenter.saveUser(accessToken.getToken());
                toastMessage = "";
                startActivity(new Intent(this , MainActivity.class));
                finish();
            } else {
                toastMessage = "Unknown response type";
            }
        }

        showToast(ToastMaker.makeToast(this , toastMessage));
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mPresenter.onConfigurationChanged(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        mPresenter.onDestroy(isChangingConfigurations());
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mPresenter.onRequestPermissionsResult(requestCode , permissions , grantResults);
    }



    /**
     * Initialize relevant MVP Objects.
     * Creates a Presenter instance, saves the presenter in {@link ActivityFragmentStatemaintainer}
     *
     * @param view
     */
    private void initialize(LoginActivityMVP.RequiredViewOps view)
            throws IllegalAccessException, InstantiationException {

        LogInPresenter presenter = new LogInPresenter(this);

        LogInModel model = new LogInModel(presenter);

        presenter.setModel(model);


        mStateMaintainer.put(LoginActivityMVP.ProvidedPresenterOps.class.getSimpleName() , presenter);
        mStateMaintainer.put(LoginActivityMVP.ProvidedModelOps.class.getSimpleName() , model);


        mPresenter = presenter ;
    }

    /**
     * Recovers Presenter and informs Presenter that occurred a config change.
     * If Presenter has been lost, recreates a instance
     */
    private void reInitialize( LoginActivityMVP.RequiredViewOps view)
            throws InstantiationException , IllegalAccessException{

        mPresenter = mStateMaintainer.get(LoginActivityMVP.ProvidedPresenterOps.class.getSimpleName());

        if(mPresenter == null)
            initialize(view);

        else
            mPresenter.onConfigurationChanged(view);
    }
}
