package gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.PickDropFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.SettingFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.R;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class SettingPresenter
    implements SettingFragmentMVP.ProvidedPresenterOps, SettingFragmentMVP.RequiredPresenterOps, SettingAdapter.OnItemClickListener {

  private WeakReference<SettingFragmentMVP.RequiredViewOps> mView;

  private SettingFragmentMVP.ProvidedModelOps mModel;

  private List<HashMap<Integer, String>> settingsItems;

  private SettingAdapter settingsAdapter;

  public SettingPresenter(SettingFragmentMVP.RequiredViewOps view) {
    setView(view);

    settingsAdapter = new SettingAdapter();
    settingsAdapter.setClickListener(this);

  }

  public void setModel(SettingFragmentMVP.ProvidedModelOps model) {
    mModel = model;
  }

  @Override
  public void setView(SettingFragmentMVP.RequiredViewOps view) {
    mView = new WeakReference<SettingFragmentMVP.RequiredViewOps>(view);
  }

  @Override
  public void onDestroy(boolean isChangingConfigurations) {
    mView = null;

    settingsItems = null ;
    mModel.onDestroy(isChangingConfigurations);

    if (!isChangingConfigurations) mModel = null;
  }

  @Override
  public void onConfigurartionChange(SettingFragmentMVP.RequiredViewOps view) {
    setView(view);
  }

  @Override
  public void showSettingsItems() {
    mModel.preparesettingsItems(getActivityContext());
  }

  @Override
  public SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater inflater = LayoutInflater.from(getActivityContext());

    View view = inflater.inflate(R.layout.settings_item, parent, false);

    SettingsViewHolder holder = new SettingsViewHolder(view);
    holder.setClickListener(this);

    return holder;
  }

  @Override
  public void onBindViewHolder(SettingsViewHolder holder, int position) {
    HashMap<Integer , String> item = settingsItems.get(position);

    for (Map.Entry<Integer , String> e : item.entrySet()){
      int resId = e.getKey() ;
      String title = e.getValue() ;

      holder.itemImageView.setImageResource(resId);
      holder.itemTitle.setText(title);
    }
  }

  @Override
  public int getItemCount() {
    return settingsItems.size();
  }

  @Override
  public Context getActivityContext() {
    Context context = null;
    try {
      context = getView().getActivityContext();
    } catch (NullPointerException e) {
      Log.e(getClass().getSimpleName() + "ERROR! ", e.getMessage());
      e.printStackTrace();
    }
    return context;
  }

  @Override
  public void onSettingItemReady(List<HashMap<Integer, String>> items) {
    settingsItems = items;
    settingsAdapter.setPresenter(this);
    getView().showSettingsItem(settingsAdapter);
  }

  private SettingFragmentMVP.RequiredViewOps getView() throws NullPointerException {
    if (mView != null) {
      return mView.get();
    } else {
      throw new NullPointerException("view is unavailable");
    }
  }

  @Override
  public void onItemClick(View itemView, int position) {

  }
}
