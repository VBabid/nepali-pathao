package gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.presenter;

import static com.google.android.gms.wearable.DataMap.TAG;
import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_DROP;
import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_PICK;

import android.app.Activity;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.os.ResultReceiver;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;

import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;

import gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility;
import gtav.pathao.nepal.flow.com.nepalipathao.common.FetchAddressIntentService;
import gtav.pathao.nepal.flow.com.nepalipathao.common.LatLngInterpolator;

import org.greenrobot.eventbus.EventBus;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.MainActivityMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.view.MainActivity;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.Utility.PermissionUtils;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ToastMaker;
import gtav.pathao.nepal.flow.com.nepalipathao.common.UserLikelyPlace;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnLocationAddressFoundEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.events.UserLocationFoundEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.presenter.PickDropFragPresenter;

/**
 * Created by VutkaBilai on 4/8/17.
 * mail : la4508@gmail.com
 */

public class MainActivityPresenter
        implements MainActivityMVP.RequiredPresenterOps, MainActivityMVP.ProvidedPresenterOps, RoutingListener {

    private WeakReference<MainActivityMVP.RequiredViewOps> mView;

    //model reference
    private MainActivityMVP.ProvidedModelOps mModel;

    // Configuration change state
    private boolean mIsChangingConfig;

    private Location mLastKnownLocation;

    private LatLngInterpolator mLatLngInterpolator;

    private Marker mPickUpmarker;
    private Marker mDropOffMarker;

    private AddressResultReciver mAddressReciver;

    private boolean shouldFetchAddress = true;

    private ProgressDialog pDialog;
    private List<Polyline> polylines;

    protected LatLng start;
    protected LatLng end;

    private static final int[] COLORS = new int[]{R.color.primary_dark, R.color.primary, R.color.primary_light, R.color.accent, R.color.primary_dark_material_light};

    /**
     * Request code for location permission request.
     *
     * @see {@linkonRequestPermissionsResult}(int, String[], int[])
     */
    public static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    public MainActivityPresenter(MainActivityMVP.RequiredViewOps mView) {

        this.mView = new WeakReference<MainActivityMVP.RequiredViewOps>(mView);

        mAddressReciver = new AddressResultReciver(new Handler());

        polylines = new ArrayList<>();
    }

    /**
     * called by activity every time during
     * setting up MVP , only called once
     */
    public void setModel(MainActivityMVP.ProvidedModelOps model) {
        this.mModel = model;
    }

    @Override
    public void onDestroy(boolean isChangingConfigurations) {
        //view should be null every time onDestroy is called
        mView = null;

        //inform model about the event
        mModel.onDestroy(isChangingConfigurations);

        mIsChangingConfig = isChangingConfigurations;
        //activity destroyed
        if (!isChangingConfigurations) {
            mModel = null;
        }
    }

    @Override
    public void onConfigurationChanged(MainActivityMVP.RequiredViewOps view) {
        setView(view);
    }

    @Override
    public void setView(MainActivityMVP.RequiredViewOps view) {
        mView = new WeakReference<MainActivityMVP.RequiredViewOps>(view);
    }

    @Override
    public boolean onCreateOptionMenu(Menu menu) {
        MenuInflater inflater = new MenuInflater(getActivityContext());
        inflater.inflate(R.menu.main_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.setting_menu) getView().showSettingsFragment();
        return true;
    }

    @Override
    public void enableMylocation() {
        if (ContextCompat.checkSelfPermission(getActivityContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission((MainActivity) getActivityContext(),
                    LOCATION_PERMISSION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else {
            // Access to the location has been granted to the app.
            if (getView().getMap() != null) {
                GoogleMap mMap = getView().getMap();
                mMap.setMyLocationEnabled(true);

                mLastKnownLocation =
                        LocationServices.FusedLocationApi.getLastLocation(getView().getClient());

                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()), 14));

                createLikelyPlace(getView().getClient());
            }
        }
    }

    private void createLikelyPlace(GoogleApiClient client) {

        if (ActivityCompat.checkSelfPermission(getActivityContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission((MainActivity) getActivityContext(),
                    LOCATION_PERMISSION_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION, true);
            return;
        }

        PendingResult<PlaceLikelihoodBuffer> result =
                Places.PlaceDetectionApi.getCurrentPlace(client, null);
        result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
            @Override
            public void onResult(@NonNull PlaceLikelihoodBuffer placeLikelihoods) {

                if (placeLikelihoods.getStatus().isSuccess() && placeLikelihoods.getCount() > 0) {
                    PlaceLikelihood likelyPlace = placeLikelihoods.get(0);

                    Place firstLikelyPlace = likelyPlace.getPlace();

                    String placeTitle = (String) firstLikelyPlace.getName();
                    LatLng placeLatLng = firstLikelyPlace.getLatLng();
                    String markerSnipet = (String) firstLikelyPlace.getAddress();
                    String placeId = firstLikelyPlace.getId();
                    if (firstLikelyPlace.getAttributions() != null) {
                        markerSnipet = markerSnipet + "\n" + firstLikelyPlace.getAttributions();
                    }

                    if (getView().getMap() != null) {

                        mLatLngInterpolator = new LatLngInterpolator.Linear();

                        mPickUpmarker = getView().getMap()
                                .addMarker(new MarkerOptions().title(placeTitle)
                                        .position(placeLatLng)
                                        .zIndex(1.0f)
                                        .snippet(markerSnipet));
                        mPickUpmarker.setIcon(
                                SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_pickup));
                        mPickUpmarker.setTag(TYPE_PICK);
                        start = mPickUpmarker.getPosition();

                        mDropOffMarker = mPickUpmarker;
                        mDropOffMarker.setTag(TYPE_DROP);
                        mDropOffMarker.setPosition(
                                new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()));

                        getView().getMap().animateCamera(CameraUpdateFactory.newLatLngZoom(placeLatLng, 16));
                        getView().getMapFragment()
                                .animateMarkerToGB(mPickUpmarker, mPickUpmarker.getPosition(), mLatLngInterpolator,
                                        1500);
                    }
                    onUserPickUpfound(placeTitle, placeLatLng, markerSnipet, placeId);
                    Log.i(TAG, "Place found: " + firstLikelyPlace.getName());
                } else {
                    Log.e(TAG, "Place not found");
                }

                placeLikelihoods.release();
            }
        });
    }

    private void onUserPickUpfound(String placeTitle, LatLng placeLatLng, String markerSnipet,
                                   String placeId) {

        UserLikelyPlace likePlace = new UserLikelyPlace();
        likePlace.setPlaceName(placeTitle);
        likePlace.setPlaceLatLng(placeLatLng);
        likePlace.setPlaceSnippet(markerSnipet);
        likePlace.setTag(TYPE_PICK);
        likePlace.setId(placeId);

        EventBus.getDefault().post(new UserLocationFoundEvent(likePlace));
    }

    private void onUserDropOfffound(String placeTitle, LatLng placeLatLng, String markerSnipet,
                                    String placeId) {

        UserLikelyPlace likePlace = new UserLikelyPlace();
        likePlace.setPlaceName(placeTitle);
        likePlace.setPlaceLatLng(placeLatLng);
        likePlace.setPlaceSnippet(markerSnipet);
        likePlace.setTag(TYPE_DROP);
        likePlace.setId(placeId);

        EventBus.getDefault().post(new UserLocationFoundEvent(likePlace));
    }

    @Override
    public void showMissingPermissionError(FragmentManager fragmentManager) {
        PermissionUtils.PermissionDeniedDialog.newInstance(true).show(fragmentManager, "dialog");
    }

    @Override
    public void updateMarker(UserLikelyPlace place) {

        if (place.getTag().equals(TYPE_PICK)) {
            if (getView().getMap() != null) {

                mPickUpmarker.remove();
                SathiUtility.hideSoftKeyboard((Activity) getActivityContext());
                mPickUpmarker = getView().getMap()
                        .addMarker(new MarkerOptions().title(place.getPlaceName())
                                .position(place.getPlaceLatLng())
                                .zIndex(1.0f)
                                .snippet(place.getPlaceSnippet()));
                mPickUpmarker.setIcon(
                        SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_pickup));
                mPickUpmarker.setTag(TYPE_PICK);
                start = mPickUpmarker.getPosition();

                getView().getMap()
                        .animateCamera(CameraUpdateFactory.newLatLngZoom(place.getPlaceLatLng(), 16));

                onUserPickUpfound(place.getPlaceName(), place.getPlaceLatLng(), place.getPlaceSnippet(),
                        place.getId());
            }

            return;
        }

        if (getView().getMap() != null) {
            if (mDropOffMarker != null) mDropOffMarker.remove();
            SathiUtility.hideSoftKeyboard((Activity) getActivityContext());
            mDropOffMarker = getView().getMap()
                    .addMarker(new MarkerOptions().title(place.getPlaceName())
                            .position(place.getPlaceLatLng())
                            .zIndex(1.0f)
                            .snippet(place.getPlaceSnippet()));
            mDropOffMarker.setIcon(
                    SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_location));
            mDropOffMarker.setTag(TYPE_DROP);
            end = mDropOffMarker.getPosition();

            getView().getMap()
                    .animateCamera(CameraUpdateFactory.newLatLngZoom(place.getPlaceLatLng(), 16));

            onUserDropOfffound(place.getPlaceName(), place.getPlaceLatLng(), place.getPlaceSnippet(),
                    place.getId());
        }
    }

    @Override
    public void removeSelectedMarker(String markerType) {
        if (markerType.equals(TYPE_PICK)) {
            mPickUpmarker.remove();
            mPickUpmarker.setPosition(
                    new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()));

            getView().showPinLocationFAB(true, TYPE_PICK);
            moveCameraForLocation(TYPE_PICK);

            return;
        }

        mDropOffMarker.remove();
        mDropOffMarker.setPosition(
                new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()));
        getView().showPinLocationFAB(true, TYPE_DROP);
        moveCameraForLocation(TYPE_DROP);
    }

    @Override
    public void moveCameraForLocation(String type) {
        switch (type) {
            case TYPE_PICK:
                if (mPickUpmarker.getPosition().latitude != mLastKnownLocation.getLatitude()) {
                    setShouldFetchAddress(false);
                    getView().getMap()
                            .animateCamera(CameraUpdateFactory.newLatLngZoom(mPickUpmarker.getPosition(), 16));

                    return;
                }

                //setShouldFetchAddress(true);
                getView().getMap()
                        .moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()),
                                14));

                break;
            case TYPE_DROP:

                if (mDropOffMarker.getPosition().latitude != mLastKnownLocation.getLatitude()) {
                    setShouldFetchAddress(false);
                    getView().getMap()
                            .animateCamera(CameraUpdateFactory.newLatLngZoom(mDropOffMarker.getPosition(), 16));
                    return;
                }

                //setShouldFetchAddress(true);
                getView().getMap()
                        .moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mLastKnownLocation.getLatitude(), mLastKnownLocation.getLongitude()),
                                14));
                getView().showPinLocationFAB(true, TYPE_DROP);

                break;
        }
    }

    @Override
    public void fetchAddress(String type, Location location) {
        Log.i(TAG, "should fetch address " + shouldFetchAddress);
        if (shouldFetchAddress) {
            Intent intent = new Intent(getActivityContext(), FetchAddressIntentService.class);
            intent.putExtra(SathiUtility.LocationConstants.RECEIVER, mAddressReciver);
            intent.putExtra(SathiUtility.LocationConstants.LOCATION_DATA_EXTRA, location);
            intent.putExtra(SathiUtility.LocationConstants.LOCATION_EDITTEXT_TYPE, type);
            getActivityContext().startService(intent);
        }
    }

    @Override
    public Context getAppContext() {
        try {

            return getView().getAppContext();
        } catch (NullPointerException e) {
            e.printStackTrace();

            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {

            return getView().getActivityContext();
        } catch (NullPointerException e) {
            e.printStackTrace();

            return null;
        }
    }

    /**
     * return the view reference.
     * could throw nullpinter exception
     * if view is null
     *
     * @return {@link MainActivityMVP.RequiredViewOps}
     */
    public MainActivityMVP.RequiredViewOps getView() throws NullPointerException {

        if (mView != null) {
            return mView.get();
        } else {
            throw new NullPointerException("view is unavailable");
        }
    }

    @Override
    public void setShouldFetchAddress(boolean shouldFetchAddress) {
        this.shouldFetchAddress = shouldFetchAddress;
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        // The Routing request failed
        if (pDialog != null)
            getView().hideProgressDialog(pDialog);

        if (e != null)
            getView().showToast(ToastMaker.makeToast(getActivityContext(), "Error " + e.getMessage()));

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(List<Route> route, int shortestRouteIndex) {

        if (pDialog != null)
            getView().hideProgressDialog(pDialog);

        CameraUpdate center =
                CameraUpdateFactory.newLatLng(mPickUpmarker.getPosition());
        CameraUpdateFactory.zoomTo(12);

        GoogleMap map = getView().getMap();
        map.moveCamera(center);


        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();

        getView().showToast(ToastMaker.makeToast(getActivityContext(), "Route " + 1
                + ": distance - " + route.get(0).getDistanceValue() + ": duration - " + route.get(0).getDurationValue()));

        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getActivityContext().getResources().getColor(R.color.primary_dark));
        polyOptions.width(10);
        polyOptions.addAll(route.get(0).getPoints());
        Polyline polyline = map.addPolyline(polyOptions);
        polylines.add(polyline);

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_pickup));
        mPickUpmarker = map.addMarker(options);
        mPickUpmarker.setTag(TYPE_PICK);
        start = mPickUpmarker.getPosition();


        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_location));
        mDropOffMarker = map.addMarker(options);
        mDropOffMarker.setTag(TYPE_DROP);
        end = mDropOffMarker.getPosition();

    }

    @Override
    public void onRoutingCancelled() {
        Log.i(TAG, "Routing was cancelled.");
    }

    public class AddressResultReciver extends ResultReceiver {

        /**
         * Create a new ResultReceive to receive results.  Your
         * {@link #onReceiveResult} method will be called from the thread running
         * <var>handler</var> if given, or from an arbitrary thread if null.
         */
        public AddressResultReciver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {

            if (resultCode == SathiUtility.LocationConstants.FAILURE_RESULT) return;

            Log.i(TAG, "location " + resultData.getString(SathiUtility.LocationConstants.LOCATION_DATA_AREA));
            EventBus.getDefault().post(new OnLocationAddressFoundEvent(resultData));

            confirmAddress(resultData.getString(SathiUtility.LocationConstants.LOCATION_EDITTEXT_TYPE),
                    resultData.getDouble(SathiUtility.LocationConstants.LOCATION_LAT),
                    resultData.getDouble(SathiUtility.LocationConstants.LOCATION_LON),
                    resultData.getString(SathiUtility.LocationConstants.LOCATION_DATA_STREET),
                    resultData.getString(SathiUtility.LocationConstants.LOCATION_DATA_AREA),
                    resultData.getString(SathiUtility.LocationConstants.LOCATION_DATA_CITY));
        }
    }

    private void confirmAddress(final String type, double lat, double lon,
                                final String street, final String area, final String city) {

        final FloatingActionButton fab = getView().getFab(R.id.fab_confirm);
        fab.setVisibility(fab.getVisibility() == View.INVISIBLE ? View.VISIBLE : View.INVISIBLE);

        final LatLng placeLatLan = new LatLng(lat, lon);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (start != null && end != null) {
                    calculateRoute();

                    return;
                }

                if (type.equals(TYPE_PICK)) {
                    Log.i(TAG, "setting marker for " + TYPE_PICK);
                    mPickUpmarker.remove();
                    mPickUpmarker = getView().getMap()
                            .addMarker(new MarkerOptions().title(street + area)
                                    .position(placeLatLan)
                                    .zIndex(1.0f)
                                    .snippet(street + area + city));
                    mPickUpmarker.setIcon(
                            SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_pickup));
                    mPickUpmarker.setTag(TYPE_PICK);

                    start = mPickUpmarker.getPosition();

                    getView().getMap()
                            .animateCamera(CameraUpdateFactory.newLatLngZoom(placeLatLan, 16));

                    fab.setVisibility(View.INVISIBLE);
                    getView().hideLocationPicker(TYPE_PICK);
                    getView().showPinLocationFAB(false, null);
                    return;
                }


                mDropOffMarker = getView().getMap()
                        .addMarker(new MarkerOptions().title(street + area)
                                .position(placeLatLan)
                                .zIndex(1.0f)
                                .snippet(street + area + city));
                mDropOffMarker.setIcon(
                        SathiUtility.getBitmapDescriptor(getActivityContext(), R.drawable.ic_location));
                mDropOffMarker.setTag(TYPE_DROP);
                end = mDropOffMarker.getPosition();

                getView().getMap()
                        .animateCamera(CameraUpdateFactory.newLatLngZoom(placeLatLan, 16));

                fab.setVisibility(View.INVISIBLE);
                getView().hideLocationPicker(TYPE_DROP);
                getView().showPinLocationFAB(false, null);
            }
        });
    }

    private void calculateRoute() {

        //pDialog = new ProgressDialog(getActivityContext() , R.style.AppTheme);
        //getView().showProgressDialog(pDialog);

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(start, end)
                .build();
        routing.execute();
    }


}
