package gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.SettingFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.R;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class SettingsModel implements SettingFragmentMVP.ProvidedModelOps {

  private SettingFragmentMVP.RequiredPresenterOps mPresenter;

  private List<HashMap<Integer, String>> settingsItems;

  public SettingsModel(SettingFragmentMVP.RequiredPresenterOps mPresenter) {
    this.mPresenter = mPresenter;
    settingsItems = new ArrayList<>();
  }

  @Override
  public void onDestroy(boolean isChangingConfigurations) {
    if (!isChangingConfigurations)
      mPresenter = null;
    settingsItems = null;

  }

  @Override
  public void preparesettingsItems(Context context) {
    TypedArray images = context.getResources().obtainTypedArray(R.array.settings_item_res);

    String[] titles = context.getResources().getStringArray(R.array.settings_item_title);

    if(settingsItems.size() == 0){
      for (int j = 0; j < titles.length; j++) {
        HashMap<Integer, String> item = new HashMap<>();

        int resId = images.getResourceId(j, -1);
        String title = titles[j];

        item.put(resId, title);

        settingsItems.add(item);
      }
    }


    mPresenter.onSettingItemReady(settingsItems);
    images.recycle();
  }
}
