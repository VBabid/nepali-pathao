package gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.view;

import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_DROP;
import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_PICK;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.google.android.gms.common.api.GoogleApiClient;

import gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnLocationAddressFoundEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnUserPlaceRemoveEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.PickDropFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.view.MainActivity;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ActivityFragmentStatemaintainer;
import gtav.pathao.nepal.flow.com.nepalipathao.events.UserLocationFoundEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.model.PickDropFragModel;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.presenter.PickDropFragPresenter;

/**
 * Created by Vutka Bilai on 4/27/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class PickupDestinationFragment extends Fragment
    implements PickDropFragmentMVP.RequiredViewOps, View.OnClickListener {

  private static final String TAG = PickupDestinationFragment.class.getSimpleName();
  private ActivityFragmentStatemaintainer mStateMaintainer;

  private MainActivity activity;

  private PickDropFragmentMVP.ProvidedPresenterOps mPresenter;

  private Unbinder unbinder;

  AutoCompleteTextView pickupAutoEt;
  AutoCompleteTextView dropAutoEt;
  ImageView clearPickUpSearch;
  ImageView clearDestinationSearch;

  private GoogleApiClient mGoogleApiClient;

  public PickupDestinationFragment() {
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);
    activity = (MainActivity) context;
    mStateMaintainer =
        new ActivityFragmentStatemaintainer(activity.getFragmentManager(), getClass().getName());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    setUpMvp();

    View view = inflater.inflate(R.layout.pick_up_drop_off_layout, null);

    pickupAutoEt = (AutoCompleteTextView) view.findViewById(R.id.pick_up_et);
    dropAutoEt = (AutoCompleteTextView) view.findViewById(R.id.drop_off_et);
    clearPickUpSearch = (ImageView) view.findViewById(R.id.clear_pickup_search);
    clearDestinationSearch = (ImageView) view.findViewById(R.id.clear_destination_search);

    mGoogleApiClient = mPresenter.configureGoogleClient();

    unbinder = ButterKnife.bind(this, view);

    return view;
  }

  @Override
  public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
    super.onViewCreated(view, savedInstanceState);

    mPresenter.setAutoCompleteAdapter(pickupAutoEt);
    mPresenter.setAutoCompleteAdapter(dropAutoEt);


    clearPickUpSearch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clearPickUpSearch.setVisibility(View.INVISIBLE);
        pickupAutoEt.setText("");
        EventBus.getDefault()
            .postSticky(new OnUserPlaceRemoveEvent(TYPE_PICK));
      }
    });

    clearDestinationSearch.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        clearDestinationSearch.setVisibility(View.INVISIBLE);
        dropAutoEt.setText("");
        EventBus.getDefault()
            .postSticky(new OnUserPlaceRemoveEvent(TYPE_DROP));
      }
    });

    pickupAutoEt.setOnClickListener(this);
    dropAutoEt.setOnClickListener(this);
  }

  private void setUpMvp() {
    try {
      if (mStateMaintainer.isFirstTimeIn()) {
        initilize(this);
      } else {
        reInitialize(this);
      }
    } catch (java.lang.InstantiationException | IllegalAccessException e) {
      Log.e(getClass().getSimpleName(), "onCreate() " + e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public Context getActivityContext() {
    return activity;
  }

  @Override
  public void showToast(Toast toast) {
    toast.show();
  }

  @Override
  public AutoCompleteTextView getAutoEditTextView(int resId) {
    return null;
  }

  @Override
  public void showClearSearch(boolean show, String type) {
    if (type.equals(TYPE_PICK)) {
      clearPickUpSearch.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
      return;
    }

    clearDestinationSearch.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
  }

  /**
   * Initialize relevant MVP Objects.
   * Creates a Presenter instance, saves the presenter in {@link ActivityFragmentStatemaintainer}
   */
  private void initilize(PickDropFragmentMVP.RequiredViewOps view)
      throws java.lang.InstantiationException, IllegalAccessException {

    //create the presenter
    PickDropFragPresenter presenter = new PickDropFragPresenter(this);

    //create the model
    PickDropFragModel model = new PickDropFragModel(presenter);

    //set presenter to model
    presenter.setModel(model);

    //save presenter
    /**and model to {@link ActivityFragmentStatemaintainer}**/
    mStateMaintainer.put(PickDropFragmentMVP.ProvidedPresenterOps.class.getSimpleName(), presenter);
    mStateMaintainer.put(PickDropFragmentMVP.ProvidedModelOps.class.getSimpleName(), model);

    //set the presenter as a interface
    //to limit communication with it
    mPresenter = presenter;
  }

  /**
   * Recovers Presenter and informs Presenter that occurred a config change.
   * If Presenter has been lost, recreates a instance
   */
  private void reInitialize(PickDropFragmentMVP.RequiredViewOps view)
      throws java.lang.InstantiationException, IllegalAccessException {

    mPresenter =
        mStateMaintainer.get(PickDropFragmentMVP.ProvidedPresenterOps.class.getSimpleName());

    if (mPresenter == null) {
      initilize(view);
    } else {
      mPresenter.onConfigurartionChange(view);
    }
  }

  @Override
  public void onPause() {
    super.onPause();
    EventBus.getDefault().unregister(this);
  }

  @Override
  public void onResume() {
    super.onResume();
    EventBus.getDefault().register(this);
  }

  @Override
  public void onStop() {
    super.onStop();
    mPresenter.onConfigurartionChange(this);

    mGoogleApiClient.disconnect();
  }

  @Override
  public void onStart() {
    super.onStart();

    mGoogleApiClient.connect();
  }

  @Override
  public void onDestroyView() {
    super.onDestroyView();
    unbinder.unbind();
    //mPresenter.onDestroy(activity.isChangingConfigurations());
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onUserLocationFoundEvent(UserLocationFoundEvent event) {
    String eventTagType = event.getUserLocation().getTag();

    if (eventTagType.equals(TYPE_PICK)) {

      clearPickUpSearch.setVisibility(View.VISIBLE);
      pickupAutoEt.setText(event.getUserLocation().getPlaceSnippet());
      return;
    }

    clearDestinationSearch.setVisibility(View.VISIBLE);
    dropAutoEt.setText(event.getUserLocation().getPlaceSnippet());
  }


  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onAddressFound(OnLocationAddressFoundEvent event){
    Bundle result = event.getResult();

    StringBuilder stringBuilder = new StringBuilder();
    String addressToDesplay = String.valueOf(stringBuilder.append(
        result.getString(SathiUtility.LocationConstants.LOCATION_DATA_STREET)
            + result.getString(SathiUtility.LocationConstants.LOCATION_DATA_AREA)
            +result.getString(SathiUtility.LocationConstants.LOCATION_DATA_CITY)));

    if(result.getString(SathiUtility.LocationConstants.LOCATION_EDITTEXT_TYPE).equals(TYPE_PICK)) {
      pickupAutoEt.setText(addressToDesplay);
      pickupAutoEt.dismissDropDown();
      showClearSearch(true , TYPE_PICK);
      return;
    }

    dropAutoEt.setText(addressToDesplay);
    dropAutoEt.dismissDropDown();
    showClearSearch(true , TYPE_DROP);
  }


  @Override
  public void onClick(View v) {
    switch (v.getId()) {
      case R.id.pick_up_et:
        mPresenter.onEditTextClick(TYPE_PICK);
        break;
      case R.id.drop_off_et:
        mPresenter.onEditTextClick(TYPE_DROP);
    }
  }
}
