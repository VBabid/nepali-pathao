package gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import gtav.pathao.nepal.flow.com.nepalipathao.R;

/**
 * Created by Vutka Bilai on 5/17/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class SettingsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

  ImageView itemImageView;
  TextView itemTitle;

  private SettingAdapter.OnItemClickListener listener ;

  public void setClickListener(SettingAdapter.OnItemClickListener listener){
    this.listener = listener;
  }

  public SettingsViewHolder(View itemView) {
    super(itemView);

    itemImageView = (ImageView) itemView.findViewById(R.id.settings_item_iv);
    itemTitle = (TextView) itemView.findViewById(R.id.tv_settings_title);

    itemView.setOnClickListener(this);
  }

  @Override
  public void onClick(View v) {
    if (listener != null) {
      int position = getAdapterPosition();
      if (position != RecyclerView.NO_POSITION) listener.onItemClick(v, position);
    }
  }
}
