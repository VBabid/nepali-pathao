package gtav.pathao.nepal.flow.com.nepalipathao.common;

import android.app.IntentService;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.os.ResultReceiver;
import android.text.TextUtils;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility;


/**
 * Created by Vutka Bilai on 6/18/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class FetchAddressIntentService extends IntentService {

  private static final String TAG = FetchAddressIntentService.class.getSimpleName() ;

  protected ResultReceiver mReciver ;
  private String locationType ;

  public FetchAddressIntentService(){
    super(TAG);
  }

  @Override
  protected void onHandleIntent(@Nullable Intent intent) {
    String errorMsg = "";

    mReciver = intent.getParcelableExtra(SathiUtility.LocationConstants.RECEIVER);

    locationType = intent.getStringExtra(SathiUtility.LocationConstants.LOCATION_EDITTEXT_TYPE);

    if(mReciver == null)
      return;

    Location location = intent.getParcelableExtra(SathiUtility.LocationConstants.LOCATION_DATA_EXTRA);

    if(location == null){
      errorMsg = getString(R.string.no_location_data_provided);
      Log.e(TAG , errorMsg);
      deliverResultToReciver(SathiUtility.LocationConstants.FAILURE_RESULT , errorMsg , null);
      return;
    }

    Geocoder geocoder = new Geocoder(this, Locale.getDefault());

    // Address found using the Geocoder.
    List<Address> addresses = null;

    try {
      // Using getFromLocation() returns an array of Addresses for the area immediately
      // surrounding the given latitude and longitude. The results are a best guess and are
      // not guaranteed to be accurate.
      addresses = geocoder.getFromLocation(
          location.getLatitude(),
          location.getLongitude(),
          // In this sample, we get just a single address.
          1);
    } catch (IOException ioException) {
      // Catch network or other I/O problems.
      errorMsg = getString(R.string.service_not_available);
      Log.e(TAG, errorMsg, ioException);
    } catch (IllegalArgumentException illegalArgumentException) {
      // Catch invalid latitude or longitude values.
      errorMsg = getString(R.string.invalid_lat_long_used);
      Log.e(TAG, errorMsg + ". " +
          "Latitude = " + location.getLatitude() +
          ", Longitude = " + location.getLongitude(), illegalArgumentException);
    }

    // Handle case where no address was found.
    if (addresses == null || addresses.size() == 0) {
      if (errorMsg.isEmpty()) {
        errorMsg = getString(R.string.no_address_found);
        Log.e(TAG, errorMsg);
      }
      deliverResultToReciver(SathiUtility.LocationConstants.FAILURE_RESULT, errorMsg, null);
    } else {
      Address address = addresses.get(0);
      ArrayList<String> addressFragments = new ArrayList<String>();

      for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
        addressFragments.add(address.getAddressLine(i));

      }
      deliverResultToReciver(SathiUtility.LocationConstants.SUCCESS_RESULT,
          TextUtils.join(System.getProperty("line.separator"), addressFragments), address);
      //TextUtils.split(TextUtils.join(System.getProperty("line.separator"), addressFragments), System.getProperty("line.separator"));

    }
  }


  private void deliverResultToReciver(int resultCode , String msg , Address address){

    try{
      Bundle bundle = new Bundle() ;
      bundle.putString(SathiUtility.LocationConstants.RESULT_DATA_KEY , msg);
      bundle.putString(SathiUtility.LocationConstants.LOCATION_DATA_AREA , address.getSubLocality());
      bundle.putString(SathiUtility.LocationConstants.LOCATION_DATA_CITY ,address.getLocality());
      bundle.putString(SathiUtility.LocationConstants.LOCATION_DATA_STREET , address.getAddressLine(0));
      bundle.putString(SathiUtility.LocationConstants.LOCATION_EDITTEXT_TYPE , locationType);
      bundle.putDouble(SathiUtility.LocationConstants.LOCATION_LAT , address.getLatitude());
      bundle.putDouble(SathiUtility.LocationConstants.LOCATION_LON , address.getLongitude());

      mReciver.send(resultCode , bundle);
    }catch (Exception e){
      e.printStackTrace();
    }

  }
}
