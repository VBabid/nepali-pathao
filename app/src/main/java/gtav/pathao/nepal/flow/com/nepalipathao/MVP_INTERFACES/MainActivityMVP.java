package gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES;

import android.app.ProgressDialog;
import android.content.Context;
import android.location.Location;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.SupportMapFragment;
import gtav.pathao.nepal.flow.com.nepalipathao.common.SyncedMapFragment;
import gtav.pathao.nepal.flow.com.nepalipathao.common.UserLikelyPlace;

/**
 * Created by VutkaBilai on 4/8/17.
 * mail : la4508@gmail.com
 */

public class MainActivityMVP {

    /**
     * Required View methods available to Presenter.
     * A passive layer, responsible to show data
     * and receive user interactions
     * (presenter -> view)
     */
    public interface RequiredViewOps{
        Context getAppContext();
        Context getActivityContext();

        void showProgressDialog(ProgressDialog dialog);
        void hideProgressDialog(ProgressDialog dialog);
        void showToast(Toast toast);
        void showAlert(AlertDialog alertDialog);

        void setLocationRequestAccepted(boolean accepted);
        void showSettingsFragment();
        GoogleMap getMap();
        SyncedMapFragment getMapFragment();
        GoogleApiClient getClient();
        void showLocationPicker(String type);
        void hideLocationPicker(String type);
        void showPinLocationFAB(boolean show ,@Nullable String type);
        FloatingActionButton getFab(int id);
    }


    /**
     * Required View methods available to Presenter.
     * A passive layer, responsible to show data
     * and receive user interactions
     * (presenter -> view)
     */
    public interface ProvidedPresenterOps{
        void onDestroy(boolean isChangingConfigurations);
        void onConfigurationChanged(RequiredViewOps view);
        void setView(RequiredViewOps view);
        boolean onCreateOptionMenu(Menu menu);
        boolean onOptionsItemSelected(MenuItem item);

        void enableMylocation();
        void showMissingPermissionError(FragmentManager fragmentManager);
        void updateMarker(UserLikelyPlace place);
        void removeSelectedMarker(String markerType);

        void moveCameraForLocation(String type);
        void fetchAddress(String type , Location location);
        void setShouldFetchAddress(boolean shouldFetchAddress);
    }


    /**
     * required Presenter operation available
     * to model (model -> presenter)
     */
    public interface RequiredPresenterOps{
        Context getAppContext();
        Context getActivityContext();
    }

    /**
     * Operations offered to model to communicate with presenter
     * Handles all data business logic
     * (presenter -> model)
     */
    public interface ProvidedModelOps{
        void onDestroy(boolean isConfigurationChanging);
    }
}
