package gtav.pathao.nepal.flow.com.nepalipathao.events;

import gtav.pathao.nepal.flow.com.nepalipathao.common.UserLikelyPlace;

/**
 * Created by Vutka Bilai on 6/1/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class OnUserDropOffLocationFoundEvent {

  UserLikelyPlace destinationLikelyPlace ;

  public OnUserDropOffLocationFoundEvent(UserLikelyPlace destinationLikelyPlace) {
    this.destinationLikelyPlace = destinationLikelyPlace;
  }

  public UserLikelyPlace getDestinationLikelyPlace() {
    return destinationLikelyPlace;
  }
}
