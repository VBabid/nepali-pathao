package gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.model;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.MainActivityMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.presenter.MainActivityPresenter;

/**
 * Created by VutkaBilai on 4/9/17.
 * mail : la4508@gmail.com
 */

public class MovieActivityModel implements MainActivityMVP.ProvidedModelOps {

    private MainActivityMVP.RequiredPresenterOps mPresenter;
    private MainActivityPresenter movieDetailPresenter;


    public MovieActivityModel(MainActivityMVP.RequiredPresenterOps mPresenter) {
        this.mPresenter = mPresenter;

        movieDetailPresenter = (MainActivityPresenter) mPresenter;
    }

    @Override
    public void onDestroy(boolean isConfigurationChanging) {
        if (!isConfigurationChanging) {
            mPresenter = null;
        }
    }
}
