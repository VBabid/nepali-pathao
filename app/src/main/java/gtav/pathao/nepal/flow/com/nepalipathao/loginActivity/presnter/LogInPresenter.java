package gtav.pathao.nepal.flow.com.nepalipathao.LoginActivity.presnter;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.facebook.accountkit.Account;
import com.facebook.accountkit.AccountKit;
import com.facebook.accountkit.AccountKitCallback;
import com.facebook.accountkit.AccountKitError;
import com.facebook.accountkit.PhoneNumber;
import com.facebook.accountkit.ui.AccountKitActivity;
import com.facebook.accountkit.ui.AccountKitConfiguration;
import com.facebook.accountkit.ui.LoginType;
import com.facebook.accountkit.ui.SkinManager;
import com.facebook.accountkit.ui.UIManager;

import java.lang.ref.WeakReference;
import java.util.HashMap;
import java.util.Map;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.LoginActivityMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility;
import gtav.pathao.nepal.flow.com.nepalipathao.rest.ApiInterface;
import gtav.pathao.nepal.flow.com.nepalipathao.rest.RestServiceGenerator;
import gtav.pathao.nepal.flow.com.nepalipathao.rest.UserloginResponse;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by VutkaBilai on 4/17/17.
 * mail : la4508@gmail.com
 */

public class LogInPresenter implements LoginActivityMVP.RequiredPresenterOps , LoginActivityMVP.ProvidedPresenterOps {


    private WeakReference<LoginActivityMVP.RequiredViewOps> mView ;

    private LoginActivityMVP.ProvidedModelOps mModel;

    public static int nextPermissionsRequestCode = 4000;

    public static final int FRAMEWORK_REQUEST_CODE = 1;

    private final Map<Integer, OnCompleteListener> permissionsListeners = new HashMap<>();

    public LogInPresenter(LoginActivityMVP.RequiredViewOps mView) {
        this.mView = new WeakReference<LoginActivityMVP.RequiredViewOps>(mView) ;

    }

    /**
     * called by activity every time during
     * setting up MVP , only called once
     *
     * @param model
     */
    public void setModel(LoginActivityMVP.ProvidedModelOps model){
        this.mModel = model ;
    }

    @Override
    public void onDestroy(boolean isChangingConfigurations) {

        mView = null ;

        mModel.onDestroy(isChangingConfigurations);

        if(!isChangingConfigurations)
            mModel = null ;
    }

    /**
     * Sent from Activity after a configuration changes
     *
     * @param view View reference
     */
    @Override
    public void onConfigurationChanged(LoginActivityMVP.RequiredViewOps view) {
        setView(view);
    }

    @Override
    public void setView(LoginActivityMVP.RequiredViewOps view) {
        mView = new WeakReference<LoginActivityMVP.RequiredViewOps>(view);

    }



    @Override
    public void onLogin(LoginType loginType) {
        final Intent intent = new Intent(getActivityContext(), AccountKitActivity.class);


        final AccountKitConfiguration.AccountKitConfigurationBuilder configurationBuilder
                = new AccountKitConfiguration.AccountKitConfigurationBuilder(
                loginType,
                AccountKitActivity.ResponseType.TOKEN);
        configurationBuilder.setDefaultCountryCode("BD");

        final AccountKitConfiguration configuration = configurationBuilder.build();

        intent.putExtra(
                AccountKitActivity.ACCOUNT_KIT_ACTIVITY_CONFIGURATION,
                configuration);



        OnCompleteListener onCompleteListener = new OnCompleteListener() {
            @Override
            public void onComplete() {
                getView().startActivityForResultWithIntent(intent , FRAMEWORK_REQUEST_CODE);
            }
        };

        if (configuration.isReceiveSMSEnabled()) {
            final OnCompleteListener receiveSMSCompleteListener = onCompleteListener;
            onCompleteListener = new OnCompleteListener() {
                @Override
                public void onComplete() {
                    requestPermissions(
                            Manifest.permission.RECEIVE_SMS,
                            R.string.permissions_receive_sms_title,
                            R.string.permissions_receive_sms_message,
                            receiveSMSCompleteListener);
                }
            };
        }
        if (configuration.isReadPhoneStateEnabled()) {
            final OnCompleteListener readPhoneStateCompleteListener = onCompleteListener;
            onCompleteListener = new OnCompleteListener() {
                @Override
                public void onComplete() {
                    requestPermissions(
                            Manifest.permission.READ_PHONE_STATE,
                            R.string.permissions_read_phone_state_title,
                            R.string.permissions_read_phone_state_message,
                            readPhoneStateCompleteListener);
                }
            };
        }

        onCompleteListener.onComplete();
    }

    private void requestPermissions(
            String receiveSms,
            int permissions_receive_sms_title,
            int permissions_receive_sms_message,
            OnCompleteListener receiveSMSCompleteListener) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (receiveSMSCompleteListener != null) {
                receiveSMSCompleteListener.onComplete();
            }
            return;
        }

        checkRequestPermissions(
                receiveSms,
                permissions_receive_sms_title,
                permissions_receive_sms_message,
                receiveSMSCompleteListener);
    }

    @TargetApi(23)
    private void checkRequestPermissions(
            final String permission,
            final int rationaleTitleResourceId,
            final int rationaleMessageResourceId,
            final OnCompleteListener listener) {
        if (getActivityContext().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED) {
            if (listener != null) {
                listener.onComplete();
            }
            return;
        }

        final int requestCode = nextPermissionsRequestCode++;
        permissionsListeners.put(requestCode, listener);

        if (getView().shouldRequestPermission(permission)) {
            new AlertDialog.Builder(getActivityContext())
                    .setTitle(rationaleTitleResourceId)
                    .setMessage(rationaleMessageResourceId)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            getView().requestForPermission(new String[] { permission }, requestCode);
                        }
                    })
                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialog, final int which) {
                            // ignore and clean up the listener
                            permissionsListeners.remove(requestCode);
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            getView().requestForPermission(new String[]{ permission }, requestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        final OnCompleteListener permissionsListener = permissionsListeners.remove(requestCode);
        if (permissionsListener != null
                && grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            permissionsListener.onComplete();
        }
    }

    @Override
    public void saveUser(String fbAccessToken) {

        TelephonyManager telManeger = (TelephonyManager) getActivityContext().getSystemService(Context.TELEPHONY_SERVICE);
        String phnNumber = telManeger.getLine1Number();

        ApiInterface apiInterface = RestServiceGenerator.createService(ApiInterface.class);

        Call<UserloginResponse> call = apiInterface.userLogin(phnNumber , fbAccessToken , "2");

        call.enqueue(new Callback<UserloginResponse>() {
            @Override
            public void onResponse(Call<UserloginResponse> call, Response<UserloginResponse> response) {
                if(response.isSuccessful()){
                    SathiUtility.saveAccessToken(getActivityContext() , response.body().getAccessToken());
                }
            }

            @Override
            public void onFailure(Call<UserloginResponse> call, Throwable t) {
                Log.d("ERROR : ","something went wrong .... "+t.getMessage());
            }
        });
    }


    /**
     * return the view reference.
     * could throw nullpinter exception
     * if view is null
     *
     * @return {@link LoginActivityMVP.RequiredViewOps}
     * @throws NullPointerException
     */

    public LoginActivityMVP.RequiredViewOps getView() throws NullPointerException {

        if (mView != null)
            return mView.get();
        else
            throw new NullPointerException("view is unavailable");
    }


    @Override
    public Context getAppContext() {
        return getView().getAppContext();
    }

    @Override
    public Context getActivityContext() {
        return getView().getActivityContext();
    }


}
