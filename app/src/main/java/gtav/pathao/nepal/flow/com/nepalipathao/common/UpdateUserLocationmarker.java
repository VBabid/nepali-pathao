package gtav.pathao.nepal.flow.com.nepalipathao.common;

import com.google.android.gms.location.places.Place;

/**
 * Created by Vutka Bilai on 6/1/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public interface UpdateUserLocationmarker {

  public void onUserLocationFound(Place place) ;

  public void  onUserDestinationFound(Place place) ;
}
