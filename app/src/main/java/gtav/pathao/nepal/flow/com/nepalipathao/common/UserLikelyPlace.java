package gtav.pathao.nepal.flow.com.nepalipathao.common;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Vutka Bilai on 5/31/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class UserLikelyPlace {

  String id;
  String placeName ;
  String placeSnippet ;
  LatLng placeLatLng ;
  String tag;

  public UserLikelyPlace() {
  }

  public String getPlaceName() {
    return placeName;
  }

  public void setPlaceName(String placeName) {
    this.placeName = placeName;
  }

  public String getPlaceSnippet() {
    return placeSnippet;
  }

  public void setPlaceSnippet(String placeSnippet) {
    this.placeSnippet = placeSnippet;
  }

  public LatLng getPlaceLatLng() {
    return placeLatLng;
  }

  public void setPlaceLatLng(LatLng placeLatLng) {
    this.placeLatLng = placeLatLng;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getTag() {
    return tag;
  }

  public void setTag(String tag) {
    this.tag = tag;
  }
}
