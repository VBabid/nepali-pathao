package gtav.pathao.nepal.flow.com.nepalipathao.events;

/**
 * Created by VutkaBilai on 6/2/17.
 * mail : la4508@gmail.com
 */

public class OnUserPlaceRemoveEvent {

  String placeType;

  public OnUserPlaceRemoveEvent(String placeTyoe) {
    this.placeType = placeTyoe;
  }

  public String getPlaceType() {
    return placeType;
  }
}
