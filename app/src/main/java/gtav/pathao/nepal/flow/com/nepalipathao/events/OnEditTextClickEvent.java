package gtav.pathao.nepal.flow.com.nepalipathao.events;

import gtav.pathao.nepal.flow.com.nepalipathao.common.UserMarkerManager;

/**
 * Created by Vutka Bilai on 6/13/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class OnEditTextClickEvent {

  String editTextType ;

  public OnEditTextClickEvent(String type) {
    this.editTextType = type;
  }

  public String getEditTextType() {
    return editTextType;
  }
}
