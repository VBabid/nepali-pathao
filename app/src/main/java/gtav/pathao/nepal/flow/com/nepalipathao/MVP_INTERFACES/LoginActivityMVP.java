package gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.widget.Toast;

import com.facebook.accountkit.ui.LoginType;

/**
 * Created by Vutka Bilai on 4/17/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class LoginActivityMVP {

    /**
     * Required View methods available to Presenter.
     * A passive layer, responsible to show data
     * and receive user interactions
     * (presenter -> view)
     */
    public interface RequiredViewOps{
        Context getAppContext();
        Context getActivityContext();

        void showProgressDialog(ProgressDialog pDialog);
        void hideProgressDialog(ProgressDialog pDialog);
        void showToast(Toast toast);
        void showAlert(AlertDialog alertDialog);

        void startActivitywithIntent(Intent intent);
        void startActivityForResultWithIntent(Intent intent , int requestCode);
        boolean shouldRequestPermission(String permission);
        void requestForPermission(String[] permissions , int requestCode);
    }


    /**
     * Required View methods available to Presenter.
     * A passive layer, responsible to show data
     * and receive user interactions
     * (presenter -> view)
     */
    public interface ProvidedPresenterOps{
        void onDestroy(boolean isChangingConfigurations);
        void onConfigurationChanged(LoginActivityMVP.RequiredViewOps view);
        void setView(LoginActivityMVP.RequiredViewOps view);

        //facebook account kit
        void onLogin(final LoginType loginType);
        void onRequestPermissionsResult (int requestCode,
                                         String[] permissions,
                                         int[] grantResults);
        void saveUser(String fbAccessToken);
    }


    /**
     * required Presenter operation available
     * to model (model -> presenter)
     */
    public interface RequiredPresenterOps{
        Context getAppContext();
        Context getActivityContext();
    }

    /**
     * Operations offered to model to communicate with presenter
     * Handles all data business logic
     * (presenter -> model)
     */
    public interface ProvidedModelOps{
        void onDestroy(boolean isConfigurationChanging);
    }
}
