package gtav.pathao.nepal.flow.com.nepalipathao.common;

import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_PICK;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.presenter.PickDropFragPresenter;
import org.greenrobot.eventbus.EventBus;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.PickDropFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.events.UserPlaceUpdateEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.view.PlaceAutoCompleteAdapter;

/**
 * Created by Vutka Bilai on 5/31/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class AutoCompleteItemClickListener
    implements AdapterView.OnItemClickListener, UpdateUserLocationmarker {

  private PlaceAutoCompleteAdapter mAdapter;
  private GoogleApiClient mClient;
  private PickDropFragmentMVP.RequiredPresenterOps mPresenter;

  String type;

  public AutoCompleteItemClickListener(PlaceAutoCompleteAdapter mAdapter, GoogleApiClient mClient,
      String type, PickDropFragmentMVP.RequiredPresenterOps presenter) {
    this.mAdapter = mAdapter;
    this.mClient = mClient;
    this.type = type;
    mPresenter = presenter;
  }

  @Override public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
    final AutocompletePrediction item = mAdapter.getItem(position);
    final String placeId = item.getPlaceId();
    final CharSequence primaryText = item.getPrimaryText(null);

    Log.i(getClass().getSimpleName(), "Autocomplete item selected: " + primaryText);

    mPresenter.updateclearSearchButton(type);

    PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi.getPlaceById(mClient, placeId);
    placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

    Log.i(getClass().getSimpleName(), "Called getPlaceById to get Place details for " + placeId);
  }

  private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback =
      new ResultCallback<PlaceBuffer>() {
        @Override public void onResult(@NonNull PlaceBuffer places) {
          if (!places.getStatus().isSuccess()) {
            // Request did not complete successfully
            Log.e(getClass().getSimpleName(),
                "Place query did not complete. Error: " + places.getStatus().toString());
            places.release();
            return;
          }

          // Get the Place object from the buffer.
          final Place place = places.get(0);


          if (type.equals(TYPE_PICK)){
            onUserLocationFound(place);
            places.release();
            return;
          }
          onUserDestinationFound(place);
          places.release();
        }
      };

  @Override public void onUserLocationFound(Place place) {
    EventBus.getDefault().post(new UserPlaceUpdateEvent(createPlace(place)));
  }

  @Override public void onUserDestinationFound(Place place) {
    EventBus.getDefault().post(new UserPlaceUpdateEvent(createPlace(place)));
  }

  private UserLikelyPlace createPlace(Place place){
    UserLikelyPlace userPlace = new UserLikelyPlace();

    userPlace.setId(place.getId());
    userPlace.setTag(type);
    userPlace.setPlaceLatLng(place.getLatLng());
    userPlace.setPlaceName((String) place.getName());
    userPlace.setPlaceSnippet((String) place.getAddress());
    if (place.getAttributions() != null) {
      userPlace.setPlaceSnippet(place.getAddress() + "\n" + place.getAttributions());
    }

    return userPlace ;
  }
}
