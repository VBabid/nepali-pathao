package gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

import com.pkmmte.view.CircularImageView;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.SettingFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.view.MainActivity;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ActivityFragmentStatemaintainer;

import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.getCircleBitmap;

import java.util.HashMap;
import java.util.List;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class SettingFragment extends Fragment implements SettingFragmentMVP.RequiredViewOps {

  private ActivityFragmentStatemaintainer mStateMaintainer;

  private SettingFragmentMVP.ProvidedPresenterOps mPresenter;

  private MainActivity activity;

  private Unbinder unbinder;
  @BindView(R.id.avater_iv)
  CircularImageView avaterImageView;
  @BindView(R.id.toolbar)
  Toolbar mToolbar;
  @BindView(R.id.settings_rv)
  RecyclerView mRecyclerView;

  public SettingFragment() {
  }

  @Override
  public void onAttach(Context context) {
    super.onAttach(context);

    activity = (MainActivity) context;
    mStateMaintainer = new ActivityFragmentStatemaintainer(activity.getFragmentManager(), getClass().getName());
  }

  @Nullable
  @Override
  public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                           @Nullable Bundle savedInstanceState) {

    setUpMvp();
    View view = inflater.inflate(R.layout.settings_fragment, container, false);
    unbinder = ButterKnife.bind(this, view);

    activity.setSupportActionBar(mToolbar);
    activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    activity.getSupportActionBar().setTitle(R.string.menu_settings);
    mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        activity.getSupportFragmentManager().popBackStack();
      }
    });

    mRecyclerView.setLayoutManager(new LinearLayoutManager(activity , LinearLayoutManager.VERTICAL , false));
    mRecyclerView.setHasFixedSize(true);

    // create bitmap from resource
    Bitmap bm = BitmapFactory.decodeResource(getResources(),
        R.drawable.demo_avatar);

    // set circle bitmap
    avaterImageView.setImageBitmap(getCircleBitmap(bm));

    mPresenter.showSettingsItems();

    return view;
  }


  private void setUpMvp() {
    try {
      if (mStateMaintainer.isFirstTimeIn()) {
        initilize(this);
      } else {
        reInitialize(this);
      }
    } catch (java.lang.InstantiationException | IllegalAccessException e) {
      Log.e(getClass().getSimpleName(), "onCreate() " + e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public Context getActivityContext() {
    return activity;
  }

  @Override
  public void showToast(Toast toast) {
    toast.show();
  }

  @Override
  public void showSettingsItem(SettingAdapter adapter) {
    mRecyclerView.setAdapter(adapter);

    adapter.notifyDataSetChanged();
  }


  /**
   * Initialize relevant MVP Objects.
   * Creates a Presenter instance, saves the presenter in {@link ActivityFragmentStatemaintainer}
   */
  private void initilize(SettingFragmentMVP.RequiredViewOps view)
      throws java.lang.InstantiationException, IllegalAccessException {

    //create the presenter
    SettingPresenter presenter = new SettingPresenter(this);

    //create the model
    SettingsModel model = new SettingsModel(presenter);

    //set presenter to model
    presenter.setModel(model);

    //save presenter
    /**and model to {@link ActivityFragmentStatemaintainer}**/
    mStateMaintainer.put(SettingFragmentMVP.ProvidedPresenterOps.class.getSimpleName(), presenter);
    mStateMaintainer.put(SettingFragmentMVP.ProvidedModelOps.class.getSimpleName(), model);

    //set the presenter as a interface
    //to limit communication with it
    mPresenter = presenter;
  }

  /**
   * Recovers Presenter and informs Presenter that occurred a config change.
   * If Presenter has been lost, recreates a instance
   */
  private void reInitialize(SettingFragmentMVP.RequiredViewOps view)
      throws java.lang.InstantiationException, IllegalAccessException {

    mPresenter = mStateMaintainer.get(SettingFragmentMVP.ProvidedPresenterOps.class.getSimpleName());

    if (mPresenter == null) {
      initilize(view);
    } else {
      mPresenter.onConfigurartionChange(view);
    }
  }
}
