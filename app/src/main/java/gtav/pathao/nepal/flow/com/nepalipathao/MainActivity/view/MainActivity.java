package gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.view;

import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_DROP;
import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_PICK;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility;
import gtav.pathao.nepal.flow.com.nepalipathao.common.FetchAddressIntentService;
import gtav.pathao.nepal.flow.com.nepalipathao.common.SyncedMapFragment;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnEditTextClickEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnUserPlaceRemoveEvent;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.MainActivityMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.model.MovieActivityModel;
import gtav.pathao.nepal.flow.com.nepalipathao.MainActivity.presenter.MainActivityPresenter;
import gtav.pathao.nepal.flow.com.nepalipathao.common.UserLikelyPlace;
import gtav.pathao.nepal.flow.com.nepalipathao.events.UserPlaceUpdateEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.presenter.PickDropFragPresenter;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.view.PickupDestinationFragment;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.Utility.PermissionUtils;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ActivityFragmentStatemaintainer;
import gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet.SettingFragment;

public class MainActivity extends AppCompatActivity
    implements MainActivityMVP.RequiredViewOps
    , OnMapReadyCallback
    , GoogleMap.OnMyLocationButtonClickListener
    , ActivityCompat.OnRequestPermissionsResultCallback
    , GoogleApiClient.ConnectionCallbacks
    , GoogleApiClient.OnConnectionFailedListener
    , GoogleMap.OnCameraIdleListener {

  // Responsible to maintain the object's integrity
  // during configurations change
  private MainActivityMVP.ProvidedPresenterOps mPresenter;
  private ActivityFragmentStatemaintainer mStateMaintainer =
      new ActivityFragmentStatemaintainer(getFragmentManager(), getClass().getName());

  private GoogleMap mMap = null;
  private SyncedMapFragment mapFragment;

  private static final String TAG = MainActivity.class.getSimpleName();

  //defauult location
  //23.603110, 89.828954

  private static final LatLng DHAKA = new LatLng(23.603110, 89.828954);

  private PickupDestinationFragment locationPickerFrag;

  Toolbar mToolbar;
  FloatingActionButton fabConfirm, fabPin;
  /**
   * Flag indicating whether a requested permission has been denied after returning in
   * {@link #onRequestPermissionsResult(int, String[], int[])}.
   */
  private boolean mPermissionDenied = false;

  // The entry point to Google Play services, used by the Places API and Fused Location Provider.
  private GoogleApiClient mGoogleApiClient;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setUpMvp();
    setContentView(R.layout.activity_main);

    mToolbar = (Toolbar) findViewById(R.id.toolbar);
    setSupportActionBar(mToolbar);
    getSupportActionBar().setTitle(R.string.order_ride_title);

    fabConfirm = (FloatingActionButton) findViewById(R.id.fab_confirm);
    fabPin = (FloatingActionButton) findViewById(R.id.fab_pin);


    fabPin.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        showLocationPicker(v.getTag().toString());
        mPresenter.setShouldFetchAddress(true);
      }
    });

    mGoogleApiClient = new GoogleApiClient.Builder(getActivityContext())
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .addApi(Places.GEO_DATA_API)
        .addApi(Places.PLACE_DETECTION_API)
        .build();
  }

  @Override
  protected void onStart() {
    super.onStart();
    Log.d(getClass().getSimpleName(), "lifecycle_event :onStart()");
    mGoogleApiClient.connect();
  }

  @Override
  protected void onResume() {
    super.onResume();
    Log.d(getClass().getSimpleName(), "lifecycle_event :onResume()");
    EventBus.getDefault().register(this);
  }

  @Override
  public void onMapReady(GoogleMap googleMap) {
    mMap = googleMap;
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DHAKA, 14));
    mMap.setOnCameraIdleListener(this);
    mMap.setMapStyle(setSelectedStyle());

    if (mapFragment != null) {
      View locationButton = ((View) mapFragment.getView()
          .findViewById(Integer.parseInt("1"))
          .getParent()).findViewById(Integer.parseInt("2"));

      locationButton.setBackgroundResource(R.drawable.tracking);

      // and next place it, on bottom right (as Google Maps app)
      RelativeLayout.LayoutParams layoutParams =
          (RelativeLayout.LayoutParams) locationButton.getLayoutParams();
      // position on right bottom
      layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
      layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
      layoutParams.setMargins(0, 0, 30, 30);

      mPresenter.enableMylocation();
    }

  }

  @Override
  public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                         @NonNull int[] grantResults) {

    if (requestCode != MainActivityPresenter.LOCATION_PERMISSION_REQUEST_CODE) return;
    if (PermissionUtils.isPermissionGranted(permissions, grantResults,
        Manifest.permission.ACCESS_FINE_LOCATION)) {
      // Enable the my location layer if the permission has been granted.
      mPresenter.enableMylocation();
    } else {
      mPermissionDenied = true;
    }
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    return mPresenter.onCreateOptionMenu(menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    mPresenter.onOptionsItemSelected(item);
    return super.onOptionsItemSelected(item);
  }

  @Override
  protected void onResumeFragments() {
    super.onResumeFragments();
    if (mPermissionDenied) {
      mPresenter.showMissingPermissionError(getSupportFragmentManager());
      mPermissionDenied = false;
    }
  }

  @NonNull
  private MapStyleOptions setSelectedStyle() {
    return MapStyleOptions.loadRawResourceStyle(this, R.raw.mapstyle_retro);
  }

  @TargetApi(Build.VERSION_CODES.KITKAT)
  private void setUpMvp() {

    try {
      if (mStateMaintainer.isFirstTimeIn()) {

        initilize(this);
      } else {
        Log.d(getClass().getSimpleName(), " reinitializing...");
        reInitialize(this);
      }
    } catch (InstantiationException | IllegalAccessException e) {
      Log.e(getClass().getSimpleName(), "onCreate() " + e);
      throw new RuntimeException(e);
    }
  }

  @Override
  public Context getAppContext() {
    return getApplicationContext();
  }

  @Override
  public Context getActivityContext() {
    return this;
  }

  @Override
  public void showProgressDialog(ProgressDialog dialog) {
    dialog.show();
  }

  @Override
  public void hideProgressDialog(ProgressDialog dialog) {
    dialog.dismiss();
  }

  @Override
  public void showToast(Toast toast) {
    toast.show();
  }

  @Override
  public void showAlert(AlertDialog alertDialog) {
    alertDialog.show();
  }

  @Override
  public void setLocationRequestAccepted(boolean accepted) {
    mPermissionDenied = accepted;
  }

  @Override
  public void showSettingsFragment() {

    SettingFragment fragment = new SettingFragment();
    getSupportFragmentManager().beginTransaction()
        .replace(R.id.main_container, fragment)
        .addToBackStack(null)
        .commit();
  }

  @Override
  public GoogleMap getMap() {

    return mMap;
  }

  @Override
  public SyncedMapFragment getMapFragment() {
    return mapFragment;
  }

  @Override
  public GoogleApiClient getClient() {
    return mGoogleApiClient;
  }

  @Override
  public void showLocationPicker(String type) {
    if (type.equals(TYPE_PICK)) {
      findViewById(R.id.drop_off_pin_holder).setVisibility(View.INVISIBLE);
      findViewById(R.id.pick_up_pin_holder).setVisibility(View.VISIBLE);
    } else {

      findViewById(R.id.pick_up_pin_holder).setVisibility(View.INVISIBLE);
      findViewById(R.id.drop_off_pin_holder).setVisibility(View.VISIBLE);
    }

  }

  @Override
  public void hideLocationPicker(String type) {
    if (type.equals(TYPE_PICK)) {
      findViewById(R.id.pick_up_pin_holder).setVisibility(View.INVISIBLE);
      return;
    }
    findViewById(R.id.drop_off_pin_holder).setVisibility(View.INVISIBLE);
  }

  @Override
  public void showPinLocationFAB(boolean show, String type) {
    fabPin.setVisibility(show ? View.VISIBLE : View.INVISIBLE);
    if (type != null)
      fabPin.setTag(type);
  }

  @Override
  public FloatingActionButton getFab(int id) {
    if (id == R.id.fab_confirm)
      return fabConfirm;

    return fabPin;
  }

  @Override
  public boolean onMyLocationButtonClick() {
    mPresenter.enableMylocation();
    return true;
  }

  @Override
  protected void onPause() {
    super.onPause();
    EventBus.getDefault().unregister(this);
  }


  @Override
  protected void onStop() {
    super.onStop();
    mGoogleApiClient.disconnect();
    mPresenter.onConfigurationChanged(this);
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    mPresenter.onDestroy(isChangingConfigurations());
  }

  /**
   * Initialize relevant MVP Objects.
   * Creates a Presenter instance, saves the presenter in {@link ActivityFragmentStatemaintainer}
   */
  private void initilize(MainActivityMVP.RequiredViewOps view)
      throws InstantiationException, IllegalAccessException {

    //create the presenter
    MainActivityPresenter presenter = new MainActivityPresenter(this);

    //create the model
    MovieActivityModel model = new MovieActivityModel(presenter);

    //set presenter to model
    presenter.setModel(model);

    //save presenter
    /**and model to {@link ActivityFragmentStatemaintainer}**/
    mStateMaintainer.put(MainActivityMVP.ProvidedPresenterOps.class.getSimpleName(), presenter);
    mStateMaintainer.put(MainActivityMVP.ProvidedModelOps.class.getSimpleName(), model);
    mStateMaintainer.put(SupportMapFragment.class.getSimpleName(), mapFragment);
    //mStateMaintainer.put(PickupDestinationFragment.class.getSimpleName(), locationPickerFrag);

    //set the presenter as a interface
    //to limit communication with it
    mPresenter = presenter;

  }

  /**
   * Recovers Presenter and informs Presenter that occurred a config change.
   * If Presenter has been lost, recreates a instance
   */
  private void reInitialize(MainActivityMVP.RequiredViewOps view)
      throws InstantiationException, IllegalAccessException {

    mPresenter = mStateMaintainer.get(MainActivityMVP.ProvidedPresenterOps.class.getSimpleName());
    mapFragment = mStateMaintainer.get(SupportMapFragment.class.getSimpleName());
    //locationPickerFrag = mStateMaintainer.get(PickupDestinationFragment.class.getSimpleName());

    if (mPresenter == null) {
      initilize(view);
    } else {
      mPresenter.onConfigurationChanged(this);
    }
  }

  @Override
  public void onConnected(@Nullable Bundle bundle) {
    if (mapFragment == null) {
      mapFragment = new SyncedMapFragment();
      getSupportFragmentManager().beginTransaction().add(R.id.map, mapFragment).commit();

      mapFragment.getMapAsync(this);
      mapFragment.setRetainInstance(true);

      locationPickerFrag = new PickupDestinationFragment();

      getSupportFragmentManager().beginTransaction()
          .add(R.id.drop_pickup_fragment, locationPickerFrag)
          .commit();
    }

  }

  @Override
  public void onConnectionSuspended(int i) {
    Log.d(TAG, "Play services connection suspended");
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
        + connectionResult.getErrorCode());
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onUserLocationUpdate(UserPlaceUpdateEvent event) {

    UserLikelyPlace place = event.getPlace();
    mPresenter.updateMarker(place);
    Log.i(TAG, "update place event ");
  }

  @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
  public void onUserLocationRemove(OnUserPlaceRemoveEvent event) {
    mPresenter.removeSelectedMarker(event.getPlaceType());
  }

  @Subscribe(threadMode = ThreadMode.MAIN)
  public void onEditTextClickEvent(OnEditTextClickEvent event) {
    mPresenter.moveCameraForLocation(event.getEditTextType());
  }


  @Override
  public void onCameraIdle() {
    LatLng cameraPosition = mMap.getCameraPosition().target;
    Log.i(TAG, "camera move stopped lat: " + cameraPosition.latitude + "lon: " + cameraPosition.longitude);

    try {
      Location location = new Location("");
      location.setLatitude(cameraPosition.latitude);
      location.setLongitude(cameraPosition.longitude);

      if (findViewById(R.id.pick_up_pin_holder).getVisibility() == View.VISIBLE)
        mPresenter.fetchAddress(TYPE_PICK, location);

      if (findViewById(R.id.drop_off_pin_holder).getVisibility() == View.VISIBLE)
        mPresenter.fetchAddress(TYPE_DROP, location);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
