package gtav.pathao.nepal.flow.com.nepalipathao.rest;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Vutka Bilai on 7/9/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class UserloginResponse {

  @SerializedName("token_type")
  private String tokenType;

  @SerializedName("expires_in")
  private long expiresTime;


  @SerializedName("access_token")
  private String accessToken;

  public String getTokenType() {
    return tokenType;
  }

  public void setTokenType(String tokenType) {
    this.tokenType = tokenType;
  }

  public long getExpiresTime() {
    return expiresTime;
  }

  public void setExpiresTime(long expiresTime) {
    this.expiresTime = expiresTime;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public void setAccessToken(String accessToken) {
    this.accessToken = accessToken;
  }
}
