package gtav.pathao.nepal.flow.com.nepalipathao.Application;

import android.app.Application;
import android.os.Build;
import android.os.StrictMode;

import gtav.pathao.nepal.flow.com.nepalipathao.Utility.FontsOverride;

/**
 * Created by VutkaBilai on 4/8/17.
 * mail : la4508@gmail.com
 */

public class PathaoApp extends Application {

    public static final String TAG = "PatahoApp" ;

    @Override
    public void onCreate() {
        super.onCreate();

        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //just to catch deeper log cases
            // if something accidentally happen
            enableStrictMode();
        }*/

        /*FontsOverride.setDefaultFont(this, "DEFAULT", "fonts/DroidSans.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/DroidSans.ttf");
        FontsOverride.setDefaultFont(this, "SERIF", "fonts/DroidSans.ttf");
        FontsOverride.setDefaultFont(this, "SANS_SERIF", "fonts/DroidSans.ttf");*/
    }


    public void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }
}
