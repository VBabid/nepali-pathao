package gtav.pathao.nepal.flow.com.nepalipathao.LoginActivity.model;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.LoginActivityMVP;

/**
 * Created by VutkaBilai on 4/17/17.
 * mail : la4508@gmail.com
 */

public class LogInModel implements LoginActivityMVP.ProvidedModelOps {

    private LoginActivityMVP.ProvidedPresenterOps mPresenter;
    private LoginActivityMVP.RequiredPresenterOps loginPresenter;


    public LogInModel(LoginActivityMVP.ProvidedPresenterOps mPresenter) {
        this.mPresenter = mPresenter;
        loginPresenter = (LoginActivityMVP.RequiredPresenterOps) mPresenter;
    }

    @Override
    public void onDestroy(boolean isConfigurationChanging) {
        if(!isConfigurationChanging){
            mPresenter = null ;
        }
    }
}
