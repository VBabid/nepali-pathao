package gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet.SettingAdapter;
import gtav.pathao.nepal.flow.com.nepalipathao.settingsFragmet.SettingsViewHolder;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class SettingFragmentMVP {

  public interface RequiredViewOps {
    Context getActivityContext();

    void showToast(Toast toast);

    void showSettingsItem(SettingAdapter adapter);
  }

  public interface ProvidedPresenterOps {

    void setView(RequiredViewOps view);

    void onDestroy(boolean isChangingConfigurations);

    void onConfigurartionChange(RequiredViewOps view);

    void showSettingsItems();

    SettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType);

    void onBindViewHolder(SettingsViewHolder holder, int position);

    int getItemCount();


  }

  public interface RequiredPresenterOps {
    Context getActivityContext();

    void onSettingItemReady(List<HashMap<Integer , String>> items);
  }

  public interface ProvidedModelOps {
    void onDestroy(boolean isChangingConfigurations);

    void preparesettingsItems(Context context);
  }
}
