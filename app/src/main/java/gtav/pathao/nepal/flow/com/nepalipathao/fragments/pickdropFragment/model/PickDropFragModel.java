package gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.model;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.PickDropFragmentMVP;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class PickDropFragModel implements PickDropFragmentMVP.ProvidedModelOps {

  private PickDropFragmentMVP.ProvidedPresenterOps mPresenter;

  public PickDropFragModel(PickDropFragmentMVP.ProvidedPresenterOps mPresenter) {
    this.mPresenter = mPresenter;
  }

  @Override public void onDestroy(boolean isChangingConfigurations) {
    if(!isChangingConfigurations)
      mPresenter = null ;
  }
}
