package gtav.pathao.nepal.flow.com.nepalipathao.events;

import android.os.Bundle;

/**
 * Created by Vutka Bilai on 6/20/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class OnLocationAddressFoundEvent {

  Bundle result;

  public OnLocationAddressFoundEvent(Bundle result) {
    this.result = result;
  }

  public Bundle getResult() {
    return result;
  }
}
