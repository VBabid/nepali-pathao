package gtav.pathao.nepal.flow.com.nepalipathao.events;

import com.google.android.gms.location.places.Place;

import gtav.pathao.nepal.flow.com.nepalipathao.common.UserLikelyPlace;

/**
 * Created by Vutka Bilai on 6/1/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public class UserPlaceUpdateEvent {

  private UserLikelyPlace place ;

  public UserPlaceUpdateEvent(UserLikelyPlace place) {
    this.place = place;
  }

  public UserLikelyPlace getPlace() {
    return place;
  }
}
