package gtav.pathao.nepal.flow.com.nepalipathao.common;

import com.google.android.gms.location.places.Place;

/**
 * Created by Vutka Bilai on 6/13/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public interface UserMarkerManager {

  interface UserLocationCallback{
    void onLocationfound(Place place);
  }

  void goToLocation(UserLocationCallback locationCallback);
}
