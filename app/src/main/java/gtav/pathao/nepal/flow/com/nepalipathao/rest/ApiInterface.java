package gtav.pathao.nepal.flow.com.nepalipathao.rest;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Vutka Bilai on 7/9/17.
 * email : la4508@gmail.com
 * project Name : nepali-pathao
 */

public interface ApiInterface {

  @POST("api/user")
  Call<UserloginResponse> userLogin(@Query("phone") String phnNumber
      , @Query("social_media_access_token") String token
      , @Query("user_type_id") String userType);
}
