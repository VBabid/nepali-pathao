package gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.presenter;

import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_DROP;
import static gtav.pathao.nepal.flow.com.nepalipathao.Utility.SathiUtility.TYPE_PICK;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.AutoCompleteTextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;

import gtav.pathao.nepal.flow.com.nepalipathao.MVP_INTERFACES.PickDropFragmentMVP;
import gtav.pathao.nepal.flow.com.nepalipathao.R;
import gtav.pathao.nepal.flow.com.nepalipathao.common.AutoCompleteItemClickListener;
import gtav.pathao.nepal.flow.com.nepalipathao.common.ToastMaker;
import gtav.pathao.nepal.flow.com.nepalipathao.common.UserMarkerManager;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnEditTextClickEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.events.OnUserPlaceRemoveEvent;
import gtav.pathao.nepal.flow.com.nepalipathao.fragments.pickdropFragment.view.PlaceAutoCompleteAdapter;

import java.lang.ref.WeakReference;
import org.greenrobot.eventbus.EventBus;

/**
 * Created by VutkaBilai on 5/16/17.
 * mail : la4508@gmail.com
 */

public class PickDropFragPresenter
    implements PickDropFragmentMVP.ProvidedPresenterOps, PickDropFragmentMVP.RequiredPresenterOps,
    GoogleApiClient.OnConnectionFailedListener {



  private WeakReference<PickDropFragmentMVP.RequiredViewOps> mView;

  private PickDropFragmentMVP.ProvidedModelOps mModel;

  private GoogleApiClient mGoogleClient;

  private PlaceAutoCompleteAdapter mAdapterPickUp;
  private PlaceAutoCompleteAdapter mAdapterDropOff;

  private AutoCompleteItemClickListener mListenerPickUp;
  private AutoCompleteItemClickListener mListenerDropOff;

  private static final LatLngBounds BOUNDS_BANGLADESH = new LatLngBounds(
      new LatLng(23.341113, 88.120798), new LatLng(25.310466, 95.674069));

  public PickDropFragPresenter(PickDropFragmentMVP.RequiredViewOps view) {
    setView(view);
  }

  @Override
  public void setView(PickDropFragmentMVP.RequiredViewOps view) {
    this.mView = new WeakReference<PickDropFragmentMVP.RequiredViewOps>(view);
  }

  public void setModel(PickDropFragmentMVP.ProvidedModelOps model) {
    mModel = model;
  }

  @Override
  public void onDestroy(boolean isChangingConfigurations) {
    mView = null;

    mModel.onDestroy(isChangingConfigurations);

    if (!isChangingConfigurations) mModel = null;
  }

  @Override
  public void onConfigurartionChange(PickDropFragmentMVP.RequiredViewOps view) {
    setView(view);

    mGoogleClient.disconnect();
  }

  @Override
  public GoogleApiClient configureGoogleClient() {

    mGoogleClient =
        new GoogleApiClient.Builder(getActivityContext()).addOnConnectionFailedListener(this)
            .addApi(Places.GEO_DATA_API)
            .build();

    return mGoogleClient;
  }

  @Override
  public void setAutoCompleteAdapter(AutoCompleteTextView view) {

    if (view.getId() == R.id.pick_up_et) {

      mAdapterPickUp = new PlaceAutoCompleteAdapter(getActivityContext(), mGoogleClient, BOUNDS_BANGLADESH,
          null);

      mListenerPickUp = new AutoCompleteItemClickListener(mAdapterPickUp, mGoogleClient , TYPE_PICK ,this);
      view.setOnItemClickListener(mListenerPickUp);
      view.setAdapter(mAdapterPickUp);
      return;
    }

    mAdapterDropOff = new PlaceAutoCompleteAdapter(getActivityContext(), mGoogleClient, BOUNDS_BANGLADESH,
        null);

    mListenerDropOff = new AutoCompleteItemClickListener(mAdapterDropOff , mGoogleClient , TYPE_DROP , this) ;
    view.setOnItemClickListener(mListenerDropOff);
    view.setAdapter(mAdapterDropOff);


  }

  @Override
  public void onEditTextClick(String type) {
    EventBus.getDefault().post(new OnEditTextClickEvent(type));
  }


  @Override
  public Context getActivityContext() {
    Context context = null;
    try {
      context = getView().getActivityContext();
    } catch (NullPointerException e) {
      Log.e(getClass().getSimpleName() + "ERROR!", e.getMessage());
      e.printStackTrace();
    }
    return context;
  }

  @Override
  public void updateclearSearchButton(String tpe) {
    getView().showClearSearch(true , tpe);
  }

  private PickDropFragmentMVP.RequiredViewOps getView() throws NullPointerException {
    if (mView != null) {
      return mView.get();
    } else {
      throw new NullPointerException("View is unavailable");
    }
  }

  @Override
  public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    Log.e(getClass().getSimpleName(),
        "onConnectionFailed: ConnectionResult.getErrorCode() = " + connectionResult.getErrorCode());

    // TODO(Developer): Check error code and notify the user of error state and resolution.
    getView().showToast(ToastMaker.makeToast(getActivityContext(),
        "Could not connect to Google API Client: Error " + connectionResult.getErrorCode()));
  }


}
